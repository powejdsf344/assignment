package company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;

import static company.constant.Constant.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Integration test")
class AssignmentTest {

    private final ByteArrayOutputStream OUT_CONTENT  = new ByteArrayOutputStream();
    private final PrintStream           ORIGINAL_OUT = System.out;
    private final String                FILE_NAME    = "src/test/resources/test.xml";


    @BeforeEach
    void setUpStreams() {
        System.setOut(new PrintStream(OUT_CONTENT));
    }

    @AfterEach
    void restoreStreams() {
        System.setOut(ORIGINAL_OUT);
    }

    @DisplayName("Test with empty comparator")
    @Test
    void printPathsFromXMLTestWithEmptyComparator() {

        String[] args = {KEY_INPUT_FILE, FILE_NAME};
        Assignment.main(args);

        List<String> expectedValue = List.of(
                "/file-776194140.xml",
                "/dir-880176375/file-1073842118.java",
                "/dir-880176375/dir-2145307015/file-1498940214.xhtml"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    @DisplayName("Test with exact comparator")
    @Test
    void printPathsFromXMLTestWithExactComparator() {

        String[] args = {KEY_INPUT_FILE, FILE_NAME, KEY_MASK, "file-1498940214.xhtml"};
        Assignment.main(args);

        List<String> expectedValue = List.of(
                "/dir-880176375/dir-2145307015/file-1498940214.xhtml"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    @DisplayName("Test with simple comparator")
    @Test
    void printPathsFromXMLTestWithSimpleComparator() {

        String[] args = {KEY_INPUT_FILE, FILE_NAME, KEY_MASK, "*.java"};
        Assignment.main(args);

        List<String> expectedValue = List.of(
                "/dir-880176375/file-1073842118.java"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    @DisplayName("Test with extended comparator")
    @Test
    void printPathsFromXMLTestWithExtendedComparator() {

        String[] args = {KEY_INPUT_FILE, FILE_NAME, KEY_MASK_REGULAR, ".*?[a-z]{4}-\\d+\\.[a-z]+"};
        Assignment.main(args);

        List<String> expectedValue = List.of(
                "/file-776194140.xml",
                "/dir-880176375/file-1073842118.java",
                "/dir-880176375/dir-2145307015/file-1498940214.xhtml"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    private List<String> getConsoleOutput() {
        return OUT_CONTENT.toString().lines().collect(Collectors.toList());
    }
}