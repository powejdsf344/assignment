package company.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import company.constant.TypeOfCompare;
import org.junit.jupiter.api.*;

@DisplayName("Command line arguments parsing")
class CommandLineArgsParserTest {

    private final ApplicationConfig     appConfig       = new ApplicationConfig();
    private final CommandLineArgsParser argumentsParser = new CommandLineArgsParser();

    @DisplayName("Test with empty input")
    @Test
    void TestEmptyCliInput() {
        assertThrows(ArgumentException.class, () -> argumentsParser.parseArguments(new String[0], appConfig));
    }

    @DisplayName("Test with unknown cli option")
    @Test

    void TestUnknownCliOption() {

        String[] commandLineArgs = {"-t", "new.xml"};
        assertThrows(ArgumentException.class, () -> argumentsParser.parseArguments(commandLineArgs, appConfig));

    }

    @DisplayName("Test with missed cli argument")
    @Test
    void TestMissedCliArgument() {

        String[] commandLineArgs = {"-f"};
        assertThrows(ArgumentException.class, () -> argumentsParser.parseArguments(commandLineArgs, appConfig));

    }

    @DisplayName("Test with correct cli arguments")
    @Test
    void TestCorrectCliArgument() {

        String[] commandLineArgs = {"-f", "new.xml", "-s", "*.java"};

        try {
            argumentsParser.parseArguments(commandLineArgs, appConfig);

            assertEquals("new.xml",     appConfig.getFileName());
            assertEquals("*.java",      appConfig.getSearchString());
            assertEquals(TypeOfCompare.SIMPLE,   appConfig.getSearchType());

        } catch (ArgumentException e) {
            fail(e.getMessage());
        }
    }
}