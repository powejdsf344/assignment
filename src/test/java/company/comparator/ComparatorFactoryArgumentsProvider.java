package company.comparator;

import company.constant.TypeOfCompare;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class ComparatorFactoryArgumentsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {

        return Stream.of(
                Arguments.of(TypeOfCompare.EMPTY,   EmptyComparator.class),
                Arguments.of(TypeOfCompare.SIMPLE,      SimpleComparator.class),
                Arguments.of(TypeOfCompare.EXTENDED,    ExtendedComparator.class)
        );

    }

}
