package company.comparator;

import static  org.junit.jupiter.api.Assertions.assertEquals;

import company.constant.TypeOfCompare;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

@DisplayName("Search factory instance test")
class ComparatorFactoryTest {

    @DisplayName("Test to create new search factory instance")
    @ParameterizedTest
    @ArgumentsSource(ComparatorFactoryArgumentsProvider.class)
    void newInstanceTest(TypeOfCompare type, Class<? extends Comparator> expectedClass){

        Comparator comparator = ComparatorFactory.getComparator(type, "test");

        Class<? extends Comparator> actualValue = comparator.getClass();

        assertEquals(expectedClass, actualValue);

    }

}