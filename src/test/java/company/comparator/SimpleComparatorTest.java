package company.comparator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

@DisplayName("Simple comparator test")
class SimpleComparatorTest {

    @DisplayName("Test correct search expressions")
    @ParameterizedTest
    @ValueSource(strings = {"*.java", "1.txt", "", "file?5*"})
    void TestSetCorrectMatcher(String searchString) {

        Assertions.assertDoesNotThrow(() ->  new SimpleComparator(searchString));

    }

}