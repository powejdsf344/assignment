package company.comparator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Empty comparator test")
class EmptyComparatorTest {

    @DisplayName("Test matches")
    @Test
    void matchesTest() {

        Comparator comparator = new EmptyComparator();

        boolean actualValue = comparator.compare("Test");

        Assertions.assertTrue(actualValue);

    }

}