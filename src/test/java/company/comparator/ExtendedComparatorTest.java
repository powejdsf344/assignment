package company.comparator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.regex.PatternSyntaxException;


@DisplayName("Extended comparator test")
class ExtendedComparatorTest {

    @DisplayName("Test correct search expressions")
    @ParameterizedTest
    @ValueSource(strings = {".*.java", "1.txt", ""})
    void TestSetCorrectMatcher(String searchString) {

        Assertions.assertDoesNotThrow(() -> new ExtendedComparator(searchString));

    }

    @DisplayName("Test incorrect search expressions")
    @ParameterizedTest
    @ValueSource(strings = {"*.java"})
    void TestSetIncorrectMatcher(String searchString) {

        Assertions.assertThrows(PatternSyntaxException.class, () -> new ExtendedComparator(searchString));

    }
}