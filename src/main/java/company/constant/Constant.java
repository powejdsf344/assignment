package company.constant;

public class Constant {
    public static final String KEY_INPUT_FILE   = "-f";
    public static final String KEY_MASK_REGULAR = "-S";
    public static final String KEY_MASK         = "-s";
    public static final String IS_FILE          = "is-file";
    public static final String NODE_NAME        = "name";
    public static final String NODE             = "node";
    public static final String CHILD_NODE       = "child";
    public static final String SPLIT_DIR        = "/";
    public static final String USAGE            = "Usage: assignment.jar -f <file.xml> [-s <filename>] [-S <filename>]\n"
            + "-f - required parameter. Specifies path to the parsed xml file\n"
            + "-s - optional parameter. Defines a simple search expression\n"
            + "-S - optional parameter. Defines an extended search expression\n\n"
            + "Parameters -s and -S can't be used at the same time.\n"
            + "If -s and -S parameters are not specified, then will be displayed paths for all files.";
}
