package company.constant;

public enum TypeOfCompare {
    EMPTY,
    SIMPLE,
    EXTENDED
}
