package company.comparator;

import company.constant.TypeOfCompare;

public class ComparatorFactory {

    public static Comparator getComparator(TypeOfCompare typeOfCompare, String searchString){

        Comparator comparator;

        switch (typeOfCompare) {
            case EMPTY:
                comparator = new EmptyComparator();
                break;
            case SIMPLE:
                comparator = new SimpleComparator(searchString);
                break;
            case EXTENDED:
                comparator = new ExtendedComparator(searchString);
                break;
            default:
                throw new EnumConstantNotPresentException(
                    typeOfCompare.getDeclaringClass(), typeOfCompare.name());
        }

        return comparator;
    }
}
