package company.path;

import company.comparator.Comparator;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class PathPrinterXML extends PathPrinter{

    public PathPrinterXML(Comparator comparator) {
        super(comparator);
    }

    public void printFromFile(String fileName)
            throws ParserConfigurationException, SAXException, IOException {

        SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
        saxParser.parse(fileName, new PathHandler(this));
    }

}
