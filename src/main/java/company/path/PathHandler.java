package company.path;

import static company.constant.Constant.CHILD_NODE;
import static company.constant.Constant.NODE;
import static company.constant.Constant.IS_FILE;
import static company.constant.Constant.NODE_NAME;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class PathHandler extends DefaultHandler {

    private       boolean     isNameElement;
    private       boolean     isFile;
    private final PathPrinter pathPrinter;

    public PathHandler(PathPrinter pathPrinter) {
        this.pathPrinter = pathPrinter;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (isNodeElement(qName)) {
            isFile = Boolean.parseBoolean(attributes.getValue(IS_FILE));
        } else if (NODE_NAME.equalsIgnoreCase(qName)) {
            isNameElement = true;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (isNameElement) {
            String fileName = new String(ch, start, length).strip();
            if (isFile) {
                pathPrinter.printPathToFile(fileName);
            } else {
                pathPrinter.addDir(fileName);
            }
            isNameElement = false;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (isNodeElement(qName)) {
            if (isFile) {
                isFile = false;
            } else {
                pathPrinter.removeLastDir();
            }
        }
    }

    private boolean isNodeElement(String Name) {
        return CHILD_NODE.equalsIgnoreCase(Name) || NODE.equalsIgnoreCase(Name);
    }
}
