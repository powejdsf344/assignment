package company.path;

import static company.constant.Constant.SPLIT_DIR;

import company.comparator.Comparator;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PathPrinter {

    private final Deque<String> dirHierarchy;
    private final Comparator    comparator;

    public PathPrinter(Comparator comparator){
        this.comparator = comparator;
        this.dirHierarchy = new ArrayDeque<>();
    }

    public void addDir(String name){
        dirHierarchy.addLast(name);
    }

    public void removeLastDir(){
        dirHierarchy.removeLast();
    }

    public void printPathToFile(String fileName){
        if (comparator.compare(fileName)) {
            System.out.println(getPathToFile(fileName));
        }
    }

    private String getPathToFile(String fileName){
        return Stream.concat(dirHierarchy.stream().map(s -> s.equals(SPLIT_DIR) ? "": s), Stream.of(fileName))
                .collect(Collectors.joining(SPLIT_DIR));
    }
}
