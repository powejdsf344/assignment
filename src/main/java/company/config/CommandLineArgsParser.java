package company.config;

import static company.constant.TypeOfCompare.SIMPLE;
import static company.constant.TypeOfCompare.EXTENDED;
import static company.constant.Constant.*;

import java.util.Arrays;
import java.util.Iterator;

public class CommandLineArgsParser {

    public void parseArguments(String[] args, ApplicationConfig config)  {

        Iterator<String> iterator = Arrays.stream(args).iterator();

        while (iterator.hasNext()){
            String opt = iterator.next();

            switch (opt) {
                case KEY_INPUT_FILE:
                    if(iterator.hasNext()) {
                        config.setFileName(iterator.next());
                    }
                    break;
                case KEY_MASK:
                    if(iterator.hasNext()) {
                        config.setSearchString(iterator.next());
                    }
                    config.setSearchType(SIMPLE);
                    break;
                case KEY_MASK_REGULAR:
                    if(iterator.hasNext()) {
                        config.setSearchString(iterator.next());
                    }
                    config.setSearchType(EXTENDED);
                    break;
                default:
                    if (!opt.isEmpty() && opt.charAt(0) == '-') {
                        throw new ArgumentException("Unknown option: '" + opt + "'\n" + USAGE);
                    }
                    break;
            }
        }

        if (config.getFileName() == null){
            throw new ArgumentException("Missing argument(s)\n" + USAGE);
        }
    }
}
