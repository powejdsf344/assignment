package company.config;

public class ArgumentException extends RuntimeException{

    public ArgumentException(String message) {
        super(message);
    }

}


