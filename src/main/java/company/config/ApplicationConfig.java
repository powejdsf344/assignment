package company.config;

import static company.constant.TypeOfCompare.EMPTY;

import company.constant.TypeOfCompare;

public class ApplicationConfig {

    private String          fileName;
    private String          searchString;
    private TypeOfCompare   typeOfCompare;

    public ApplicationConfig(){
        this.typeOfCompare = EMPTY;
    }

    public String getFileName() {
        return fileName;
    }

    public String getSearchString() {
        return (searchString == null) ? "" : searchString;
    }

    public TypeOfCompare getSearchType() {
        return typeOfCompare;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public void setSearchType(TypeOfCompare typeOfCompare) {
        this.typeOfCompare = typeOfCompare;
    }

}
